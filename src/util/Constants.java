package util;

import java.nio.file.Path;
import java.nio.file.Paths;

/***
 * Used constants for the program. 
 * @author Thomas Volkmann 
 */
public class Constants 
{
	// Fixed Size for the histogram creation in Metric
	public static final int BINSIZE = 10;
	// distance from current point for grouping
	public static final int THRESHOLD = 20; // 40 for ...2 / 20
	// Points cloud minimum value
	public static final int MINGROUPSIZE = 200;	// 400 for ...2  / 200
	// procedural value for the difference level used in ImageTransformation
	public static final int PERCENTAGE_DIFFERENCE = 10;
	// value for the learning algorithm allowed values are between 0.0 - 1.0
	// default is 0.2
	public static final double LEARNING_RATE = 0.2;
	// working directory	
	public static final Path CURRENT_RELATIVE_PATH = Paths.get("");
	// absolute Path for string concatenation 
	public static final String ABSOLUTE_PATH = CURRENT_RELATIVE_PATH.toAbsolutePath().toString();
	// picture format for saving
	public static final String FORMAT = "png";
	// value for the height of the image
	public static final int IMAGE_HEIGHT = 800; // 1500 for ...2 / 800
	// value for the width of the image
	public static final int IMAGE_WIDTH = 800; // 800 for ...2 / 800
	// string to the directory where the pictures for learning are saved
	public static final String LEARNING_TRAINING_DIRECTORY = ABSOLUTE_PATH + "/pictures/training";
	// string to the directory where the pictures for testing are saved
	public static final String TESTING_TRAINING_DIRECTORY = ABSOLUTE_PATH + "/pictures/testing";
	// string to the directory where the pictures for validation are saved
	public static final String VALIDATION_TRAINING_DIRECTORY = ABSOLUTE_PATH + "/pictures/validation";
	// string array of allowed file extensions
	static final String[] EXTENSIONS = new String[]	{"jpg",	"png", "bmp" , "jpeg"};
}

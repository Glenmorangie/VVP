// Copyright © Diego Catalano, 2015
// diego.catalano at live.com
//
// Copyright © Andrew Kirillov, 2007-2008
// andrew.kirillov at gmail.com
//
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

package machineLearning;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import image.AverageMetric;
import util.Helper;

/**
 * Base neural network class.
 * @author Diego Catalano edited by Thomas Volkmann
 */
public abstract class Network implements Serializable
{    
    /**
	 * Auto generated serial number.
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Network's layers count.
     */
    protected int layersCount;
    
    /**
     * Network's layers.
     */
    public Layer layer;

    /**
     * Get Network's inputs count.
     * @return Network's inputs count.
     */
    public int getInputsCount() 
    {
        return inputsCount;
    }
    
    /***
     * Get the average Metric which is saved in the network for further use.
     * @return Map average Metric
     */
    public Map<Integer, AverageMetric> getAverageMetric()
    {
    	return averMet;
    }
    
    /**
     * Get Network's layers count.
     * @return Network's layers count.
     */
    public Layer getLayers() 
    {
        return layer;
    }

    /**
     * Get Network's output vector.
     * @return Network's output vector.
     */
    public ArrayList<Double> getOutput() 
    {
        return output;
    }
    
    /**
     * Initializes a new instance of the Network class.
     * @param inputsCount Network's inputs count.
     * @param layersCount Network's layers count.
     */
    protected Network( int inputsCount)
    {    	
        this.inputsCount = Math.max( 1, inputsCount );
        
        // create collection of layers
        this.layer = Layer.getInstance(inputsCount);
    }
    
    /**
     * Compute output vector of the network.
     * @param input Input vector.
     * @return Returns network's output vector.
     */
    public ArrayList<Double> Compute(ArrayList<Integer> input)
    {
        // local variable to avoid mutlithread conflicts
		ArrayList<Double> output = new ArrayList<>();

        output.addAll(layer.Compute(input));

        return output;
    }
    
    /**
     * Compute output vector of the network.
     * @param input Input vector.
     * @return double network's output vector summarized.
     */
    public String checkForRecognition(ArrayList<Integer> input)
    {
		return Helper.numberConverter(layer.computeSum(input));
    }
    
    /**
     * Save network to specified file.
     * @param fileName File name to save network into.
     * @param averMetric average metric to save
     */
    public void Save(String fileName, Map<Integer, AverageMetric> averMetric)
    {
    	this.averMet = averMetric;
        try 
        {
        	ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(this);
            out.close();
        } 
        catch (FileNotFoundException ex) 
        {
            ex.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * Load network from specified file.
     * @param fileName File name to load network from.
     * @return Returns instance of Network class with all properties initialized from file.
     */
    public Network Load(String fileName)
    {
        Network network = null;
        try 
        {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
            network = (Network)in.readObject();
            in.close();
        } 
        catch (FileNotFoundException ex) 
        {
            ex.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return network;
    }
    	
    /***
     * Saved average metric of all seen Images.
     */
	private Map<Integer, AverageMetric> averMet;

	/**
     * Network's inputs count.
     */	
    private int inputsCount;

    /**
     * Network's output vector.
     */
    private ArrayList<Double> output;
}
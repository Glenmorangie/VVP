package main;

import java.util.ArrayList;

import image.PatternHelper;
import machineLearning.ActivationNetwork;
import machineLearning.BipolarSigmoidFunction;
import machineLearning.PerceptronLearning;
import pre_processing.ImageTransformation;
import util.Constants;

public class main 
{	
	public static void main(String[] args) 
	{	
		ImageTransformation it = new ImageTransformation(Constants.LEARNING_TRAINING_DIRECTORY);
		
		ArrayList<PatternHelper> patternList = new ArrayList<>();
		patternList = it.computeAverageMetric();
		
		System.out.println("Found Pattern");
		for (PatternHelper element : patternList)
		{
			int index = element.getSize();
			for (int ind = 0; ind < index; ind++)
				System.out.print(element.getElement(ind) + " ");
			System.out.println(element.getTagName());
		}
		
		// network initialization
		ActivationNetwork an = new ActivationNetwork(new BipolarSigmoidFunction(), it.getCurator().get(0).getMetricCurator().metricList.size());

		PerceptronLearning pl = new PerceptronLearning(an);
		pl.setLearningRate(0.2);		
		
		double errorrate = 0; 
		
		for (int index = 0; index < 2; index++)
		{
			for (PatternHelper pattern : patternList)
			{
				errorrate =	pl.Run(pattern.getPatternList());
				//System.out.println("ErrorRate: " + errorrate);
			}
		}
		
		an.Save("Network", it.getAverageMetric());
		
		// "empty" initializing for the loaded network
		ActivationNetwork an2 = new ActivationNetwork(new BipolarSigmoidFunction(), 1);
		an2 = (ActivationNetwork) an2.Load("Network");
				
		System.out.println("Loaded Network");

		ImageTransformation it2 = new ImageTransformation(Constants.TESTING_TRAINING_DIRECTORY, an2.getAverageMetric() , false);
		
		ArrayList<PatternHelper> patternList2 = new ArrayList<>();
		patternList2 = it2.computeAverageMetric();
		
		System.out.println("Found Pattern");
		for (PatternHelper element : patternList2)
		{
			int index = element.getSize();
			for (int ind = 0; ind < index; ind++)
				System.out.print(element.getElement(ind) + " ");
			System.out.println(element.getTagName());
		}
		
		for (PatternHelper pattern : patternList2)
		{
			System.out.println(an2.checkForRecognition(pattern.getPatternList()));
		}		
		System.out.println("Validation");	
		
		ImageTransformation it3 = new ImageTransformation(Constants.VALIDATION_TRAINING_DIRECTORY, an2.getAverageMetric() ,false);
		
		ArrayList<PatternHelper> patternList3 = new ArrayList<>();
		patternList3 = it3.computeAverageMetric();
				
		System.out.println("Found Pattern");
		for (PatternHelper element : patternList3)
		{
			int index = element.getSize();
			for (int ind = 0; ind < index; ind++)
				System.out.print(element.getElement(ind) + " ");
			System.out.println(element.getTagName());
		}
		
		for (PatternHelper pattern : patternList3)
		{			
			String test = an2.checkForRecognition(pattern.getPatternList());
			if (Double.parseDouble(test) > 0)
			{
				System.out.println(test + " " +pattern.getTagName());
			}
			else
			{
				System.err.println(pattern.getTagName());
			}
		}
	}
}